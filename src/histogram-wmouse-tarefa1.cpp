//system include
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
//opencv include
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

//G Var
using namespace std;
using namespace cv;
#define w 512
#define h 400
//Functions
void MyLine( Mat img, Point start, Point end );
void mouseCallBack(int event, int x, int y, int flags, void *userdata);
void showHelp(void);



int main(int argc, char** argv)
{
    string wname;
   	wname = "Histograma com Mouse";

    //Call File needed
    CommandLineParser parser( argc, argv, "{@input | ../imagens/teste.png | input image}" );
    Mat src = imread( samples::findFile( parser.get<String>( "@input" ) ), IMREAD_COLOR );
    if( src.empty() )
    {
        return EXIT_FAILURE;
    }
    vector<Mat> bgr_planes;
    split( src, bgr_planes );
    int histSize = 256;
    float range[] = { 0, 256 }; //the upper boundary is exclusive
    const float* histRange = { range };
    bool uniform = true, accumulate = false;
    Mat b_hist, g_hist, r_hist;
    calcHist( &bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );
    int hist_w = 15*w/16, hist_h = 15*h/16 -1;
    int bin_w = cvRound( (double) hist_w/histSize );
    Mat histImage( h, w, CV_8UC3, Scalar( 0,0,0) );
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    for( int i = 1; i < histSize; i++ )
    {
        line( histImage, Point( 1+bin_w*(i-1)+ w/16, hist_h - cvRound(b_hist.at<float>(i-1)) ),
              Point( 3+bin_w*(i)+ w/16, hist_h - cvRound(b_hist.at<float>(i)) ),
              Scalar( 255, 0, 0), 2, 8, 0  );
        line( histImage, Point( 1+bin_w*(i-1)+ w/16, hist_h - cvRound(g_hist.at<float>(i-1)) ),
              Point( 3+bin_w*(i)+ w/16, hist_h - cvRound(g_hist.at<float>(i)) ),
              Scalar( 0, 255, 0), 2, 8, 0  );
        line( histImage,
             Point( 1+bin_w*(i-1)+ w/16, hist_h - cvRound(r_hist.at<float>(i-1)) ),
              Point( 3+bin_w*(i)+ w/16, hist_h - cvRound(r_hist.at<float>(i)) ),
              Scalar( 0, 0, 255), 2, 8, 0  );

    }
    //Grafico X-Y
    MyLine( histImage, Point( w/16, 15*h/16 ), Point( 15*w/16, 15*h/16 ) ); // X-Linha
    MyLine( histImage, Point( w/16, h/16 ), Point( w/16, 15*h/16 ) ); //Y-Linha
    putText( histImage, "X", Point(15*w/16,63*h/64), FONT_HERSHEY_COMPLEX, 0.5,
           Scalar(150, 150, 150), 1.25, 64 );
    putText( histImage, "Y", Point(w/64, h/16), FONT_HERSHEY_COMPLEX, 0.5,
           Scalar(150, 150, 150), 1.25, 64 );
    putText( histImage, "(0,0)", Point(w/64, 63*h/64), FONT_HERSHEY_COMPLEX, 0.5,
           Scalar(150, 150, 150), 1.25, 64 );


    //MMouse////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Mat		image;		// Image object to be used
    Mat		tmp;			// Temporary image
    string	filename;	// Image name
    int		h2,w2,height, width;
    char		ch;
	image = histImage;

   	// Create callback to handle mouse events
	setMouseCallback(wname.c_str(), mouseCallBack, &image);

	// Create a window for display.
	namedWindow( wname.c_str(), WINDOW_NORMAL);

	// Create callback to handle mouse events
	setMouseCallback(wname.c_str(), mouseCallBack, &image);

	// Resize windows and image will fit in
	width = image.cols;
	height = image.rows;
   w2 = width*2;
   h2 = height*2;

	// Resize the window to width 800xh
   resizeWindow( wname.c_str(), w2, h2);

	// Show our image inside it.
	imshow( wname.c_str(), image );

	tmp = image.clone();			// Create a new copy of image

	do {
		ch = waitKey(1);			// Wait for a key press
										// If the parameter differ from 0
										// wait for the given time
										// If equal 0, wait indefinitely
										//		for the pressed key
		switch( ch ) {
			case 'b':
			case 'B': {
				tmp = tmp > 128;
				break;
			}
			case 'r':
			case 'R':
				tmp = image.clone();
				break;
			case 'h':
			case 'H':
			case '?':
				showHelp();
				break;
		}
		imshow( wname.c_str() , tmp );
	} while ( ch != 27 );		// Wait until <ESC> was pressed
	cout << endl;


    waitKey();
    return EXIT_SUCCESS;
}
void MyLine( Mat img, Point start, Point end ){
  int thickness = 1;
  int lineType = LINE_8;
  line( img,
    start,
    end,
    Scalar( 220, 220, 220 ),
    thickness,
    lineType );
}

void mouseCallBack(int event, int x, int y, int flags, void *userdata){
string msg;
int delta;
Mat tmp = *(static_cast<Mat *>(userdata));

	switch ( event ){
		case EVENT_MOUSEMOVE:
			msg = "Mouse moving";
			break;
		case EVENT_LBUTTONDOWN:
			msg = "Left Down";
			break;
		case EVENT_LBUTTONUP:
			msg = "Left Up";
			break;
		case EVENT_RBUTTONDOWN:
			msg = "Rigth Down";
			break;
		case EVENT_RBUTTONUP:
			msg = "Rigth UP";
			break;
		case EVENT_MBUTTONDOWN:
			msg = "Middle Down";
			break;
		case EVENT_MBUTTONUP:
			msg = "Middlw UP";
			break;
		case EVENT_LBUTTONDBLCLK:
			msg = "Double click Left";
			break;
		case EVENT_RBUTTONDBLCLK:
			msg = "Double click Rigth";
			break;
		case EVENT_MBUTTONDBLCLK:
			msg = "Double click Middle";
			break;
		case EVENT_MOUSEWHEEL:
			msg = "Mouse Wheel (+)";
			break;
		case EVENT_MOUSEHWHEEL:
			msg = "Mouse Wheel (-)";
			break;
	}
	Vec3b v = tmp.at<Vec3b>(y,x);
	cout << msg << ">> [" <<
			setw(3) << hex << event << "][" <<
			setw(3) << hex << flags << "]"<<
			setw(3) << dec << delta <<
			setw(2) << "(" << setw(4) << x << "," << setw(4) << y << ")" <<
			" RGB("
					<< setw(3) << (int)(v[2]) << ", "
					<< setw(3) << (int)(v[1]) << ", "
					<< setw(3) << (int)(v[0]) << " )"
			<< setw(11) << '\r' << flush;
}

void showHelp(void){

	cout << "\n\
Mouse Example Version 1.0.0\n\n\
This examples show how mouse works\n\
\tKeys\tActions\n\n\
\t<b>\tTransform image to binary\n\
\t<h>\tThis help screen\n\
\t<r>\tReset to original image\n\
\t<ESC>\tQuit\n\n";

}
