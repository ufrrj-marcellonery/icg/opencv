#include <opencv2/core/version.hpp>
#include <opencv2/imgproc.hpp>

#ifndef CV_VERSION_MAJOR    4
#define COLOR_BGR2GRAY  CV_BGR2GRAY 
#define IMREAD_COLOR    CV_LOAD_IMAGE_COLOR 


#endif // CV_VERSION_MAJOR

